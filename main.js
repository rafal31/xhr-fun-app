//variables
const apiBase = "https://swapi.dev/api/";
const peopleSwapi = "people/";
const favoriteStorage = "https://api.npoint.io/aabb252bccb7d0b9444d"; // https://www.npoint.io/docs/aabb252bccb7d0b9444d
let listOfPeople = localStorage.getItem("SWAPI")
  ? JSON.parse(localStorage.getItem("SWAPI"))
  : null;
let listOfFavorite = [];
const peopleList = document.querySelector(".people-list");
const favoriteList = document.querySelector(".favorite-list");
const browse = document.querySelector(".browse");
const favButton = document.querySelector(".fav-button");
const favMobile = document.querySelector(".favorite-container");
const favClose = document.querySelector(".favorite-close");
const backdrop = document.querySelector('.backdrop');


//execution

fetchListOfPeople(); //Promise z .then
fetchListOfFavorite();

//wyszukiwarka osob
browse.addEventListener("input", (e) => {
  let search = document.querySelector("input").value.toUpperCase();
  filterPeople(search);
});

//rozwiajnie szczegolow albo dodawanie do ulubionych
peopleList.addEventListener("click", (e) => {
  if (e.target.id) {
    const personId = e.target.id;
    toggleDetails(personId);
  }else if (e.target.tagName == 'LABEL') { 
    const personId = e.path[1].id;
    toggleDetails(personId);
  } else if (e.target.dataset.id) {
    const personId = e.target.dataset.id;
    addToFavorites(personId);
  }
});


//usuwanie z ulubionych
favoriteList.addEventListener("click", (e) => {
  if (e.target.dataset.id) {
    const personId = e.target.dataset.id;
    let delFavorite = listOfFavorite.filter((person) => {
      return person.id != personId;
    });

    if (
      confirm(`Czy chcesz usunąć z ulubionych ${listOfPeople[personId].name}?`)
    ) {
      removeFavorite(delFavorite);
      const selectedPerson = document.querySelector(`.star${personId}`);
      selectedPerson.classList.toggle("star-checked");
    }
  }
});
//mobilne otwieranie
favButton.addEventListener("click", (e) => {
  favMobile.classList.toggle("fav-open");
  backdrop.classList.toggle('visible');
});
//mobilen zamykanie
backdrop.addEventListener("click", (e) => {
  favMobile.classList.toggle("fav-open");
  backdrop.classList.toggle('visible');
})

favClose.addEventListener("click", (e) => {
  favMobile.classList.toggle("fav-open");
  backdrop.classList.toggle('visible');
});

//DECLARATION
//pobieranie listy z serwera jak nic nie ma / renderuj
function fetchListOfPeople() {
  if (!listOfPeople) {
    return sendHttpRequest("GET", apiBase + peopleSwapi)
      .then((date) => {
        date.results.map((person, index) => {
          return (person.id = index);
        });
        listOfPeople = date.results;
        storePeople(listOfPeople);
        renderListOfPeople(listOfPeople);
      })
      .catch((error) => {
        alert(error.message);
      });
  } else renderListOfPeople(listOfPeople);
}

//pobieranie listy z serwera jak nic nie ma / renderuj
function fetchListOfFavorite() {
  return sendHttpRequest("GET", favoriteStorage)
    .then((date) => {
      listOfFavorite = date;
      toggleStars(listOfFavorite);
      renderListOfFavorite(listOfFavorite);
    })
    .catch((error) => {
      alert(error.message);
    });
}

//xhr
function sendHttpRequest(method, url, data) {
  const promise = new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();

    xhr.open(method, url);
    xhr.responseType = "json";
    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response);
      } else {
        reject(new Error("Something went wrong!"));
        console.log(xhr.response);
        console.log(xhr.status);
      }
    };
    xhr.onerror = () => {
      reject(new Error("Failed to send request!"));
      console.log(xhr.response);
      console.log(xhr.status);
    };
    xhr.send(data ? JSON.stringify(data) : null);
  });
  return promise;
}

//zapisz do LS
function storePeople(listOfPeople) {
  window.localStorage.setItem("SWAPI", JSON.stringify(listOfPeople));
}

//algorytm wyszukiwania
function filterPeople(search) {
  if (search) {
    let browsePeople = listOfPeople.filter((person) => {
      return person.name.toUpperCase().indexOf(search) > -1;
    });
    renderListOfPeople(browsePeople);
    toggleStars(listOfFavorite);
  } else {
    renderListOfPeople(listOfPeople);
    toggleStars(listOfFavorite);    
  }
}

//przekazywanie
function renderListOfPeople(listOfPeople) {
  const stringPeople = getPersonHtml(listOfPeople);
  peopleList.innerHTML = stringPeople;
}

//stworz html
function getPersonHtml(listOfPeople) {
  let peopleHtmlStrings = listOfPeople.map((person) => {
    return `<li>
      <div class="person-name" id="${person.id}"> 
        <input type="radio" name="swapi" class="accorderon" id="${person.name}">
        <label for="${person.name}" >${person.name}
          <img class="star star${person.id}" data-id="${person.id}" src="https://image.flaticon.com/icons/png/512/1828/1828970.png" alt="favorit-star">
        </label>
      </div>
      <div class="details index-${person.id}">
        <div><strong>Height: </strong>${person.height}cm</div>
        <div><strong>Mass: </strong>${person.mass}kg</div>
        <div><strong>Hair color: </strong>${person.hair_color}</div>
        <div><strong>Skin color: </strong>${person.skin_color}</div>
        <div><strong>Eye color: </strong>${person.eye_color}</div>
        <div><strong>Birth year: </strong>${person.birth_year}</div>
        <div><strong>Gender: </strong>${person.gender}</div>
        <div><strong>Homeworld: </strong>${person.homeworld} </div>
        <div><strong>Films: </strong>${person.films} </div>
        <div><strong>Species: </strong>${person.species} </div>
        <div><strong>Vehicles: </strong>${person.vehicles} </div> 
        <div><strong>Starships: </strong>${person.starships} </div>
        <div><strong>Created: </strong>${person.created}</div>
        <div><strong>Edited: </strong>${person.edited}</div>
      </div>
    </li>`;
  });
  return peopleHtmlStrings.join(""); 
}

//rozin liste
function toggleDetails(personId) {
  const personName = document.getElementsByClassName('person-name');
  for (let i = 0; i < personName.length; i++) {
    if (personId == personName[i].id) {
      const selectedPerson = document.querySelector(`.index-${personId}`);
      selectedPerson.classList.toggle("details-display");      
    } else if (personId != personName[i].id) {
      const othersPerson = document.querySelector(`.index-${personName[i].id}`);
      othersPerson.classList.remove("details-display");      
    }
  }
}

//dodwanie do fav
function addToFavorites(personId) {
  let checkedFavorite = listOfFavorite.filter((person) => {
    return person.id == listOfPeople[personId].id;
  })[0];

  if (checkedFavorite) {
    return alert("Lubisz to!");
  } else {
    const selectedPerson = document.querySelector(`.star${personId}`);
    selectedPerson.classList.toggle("star-checked");
    storeFavorite(listOfPeople[personId]);
  }
}

//html do fav
function getFavoritHtml(listOfFavorite) {
  let favoriteHtmlStrings = listOfFavorite.map((person) => {
    return `<li>
      <div><h3 id="${person.id}">${person.name} <img data-id="${person.id}" class="delete delete${person.id}" src="http://simpleicon.com/wp-content/uploads/trash.png" alt="favorit-star"></h3></div>
      </li>`;
  });
  return favoriteHtmlStrings.join("");
}

//render
function renderListOfFavorite(listOfFavorite) {
  const stringFavorite = getFavoritHtml(listOfFavorite);
  favoriteList.innerHTML = stringFavorite;
}

//zapisywanie na serwer
function storeFavorite(newFav) {
  let copyOfFavorite = listOfFavorite.concat([newFav]);
 
  return sendHttpRequest("POST", favoriteStorage, copyOfFavorite)
    .then(() => {
      listOfFavorite.push(newFav);
      renderListOfFavorite(listOfFavorite);
    })
};
  
//usuwanie favorite
function removeFavorite(delFavorite) {
  return sendHttpRequest("POST", favoriteStorage, delFavorite)
    .then(() => {
      listOfFavorite = delFavorite;
      renderListOfFavorite(listOfFavorite);
    })
};

function toggleStars(listOfFavorite) {
  for(person of listOfFavorite) {
    const selectedPerson = document.querySelector(`.star${person.id}`);
    selectedPerson.classList.toggle("star-checked");
  }
}
